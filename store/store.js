

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const state = () => ({
    isReduced: true
  })

export const actions = {
    updateReduce({commit}){
        commit('updataReduce')
    },
}

export const getters = {
    isReducedOrNot: state => {
      return state.isReduced ? true : false
    }
  }

export const mutations = {
    updataReduce(state){
        state.isReduced = !state.isReduced
      },
}


